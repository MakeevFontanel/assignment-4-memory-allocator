#ifndef TESTER_H
#define TESTER_H

#include <stdint.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void run_tests();
static bool test_1(struct block_header* heap);
static bool test_2(struct block_header* heap);
static bool test_3(struct block_header* heap);
static bool test_4(struct block_header* heap);
static bool test_5(struct block_header* heap);

#endif