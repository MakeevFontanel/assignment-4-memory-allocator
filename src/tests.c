#define _DEFAULT_SOURCE

#include "tests.h"

#include <unistd.h>




enum Memory {
    KILOBYTE = 1024,
    MEGABYTE = 8388608
};

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

static bool(*tests_array[5])(struct block_header*) = {&test_1 , &test_2 , &test_3 , &test_4 , &test_5};

void* print_and_malloc(size_t memory) {
    void *ptr = _malloc(memory);
    if (ptr == NULL){
        printf("No memory allocated!\n");
        return NULL;
    }
    printf("Allocated %zu bytes\n", memory);
    return ptr;
}


static int count_blocks(struct block_header* block) {
    int counter = 0;
    struct block_header *iterator = block;
    while (iterator->next) {
        if (!iterator->is_free) counter++;
        iterator = iterator->next;
    }
    return counter;
}

static void* init_heap() {
    void *heap = heap_init(KILOBYTE*8);
    if (heap == NULL) {
        err("Heap is not initialised!");
    }
    printf("Heap initialized successfully!\n");
    return heap;
}

static bool test_1(struct block_header* heap) {
    void *data = print_and_malloc(KILOBYTE * 2);
    if (data == NULL) {
        fprintf(stderr , "Allocated memory points to NULL!\n");
        return false;
    }

    debug_heap(stdout, heap);

    if (heap->capacity.bytes != KILOBYTE*2) {
        printf("Capacity is free!\n");
        return false;
    }
    _free(data);
    return true;
}

static bool test_2(struct block_header* heap) {
    debug_heap(stdout, heap);
    void *block_1 = print_and_malloc(KILOBYTE * 3);
    void *block_2 = print_and_malloc(KILOBYTE * 4);
    debug_heap(stdout, heap);
    if (count_blocks(heap) != 2) {
        _free(block_1);
        _free(block_2);
        printf("Amount of allocated block is incorrect\n");
        return false;
    }
    _free(block_1);
    if (count_blocks(heap) != 1) {
        printf("Memory is not freed correctly\n");
        _free(block_2);
        return false;
    }
    _free(block_2);
    return true;
}

static bool test_3(struct block_header* heap) {
    void *block_1 = print_and_malloc(KILOBYTE);
    void *block_2 = print_and_malloc(KILOBYTE * 2);
    void *block_3 = print_and_malloc(KILOBYTE * 3);
    if (block_1 == NULL || block_2 == NULL || block_3 == NULL) {
        return false;
    }

    debug_heap(stdout, heap);

    if (count_blocks(heap) != 3) {
        _free(block_1);
        _free(block_2);
        _free(block_3);
        printf("Amount of allocated block is incorrect\n");
        return false;
    } else {
        _free(block_1);
        _free(block_2);
        debug_heap(stdout, heap);
        if (count_blocks(heap) == 1){
            _free(block_3);
            return true;
        }
        else {
            printf("Memory is not freed correctly\n");
            _free(block_3);
            return false;
        }
    }
}

static bool test_4(struct block_header* heap) {
    debug_heap(stdout, heap);
    void *block_1 = print_and_malloc(KILOBYTE * 15);
    void *block_2 = print_and_malloc(KILOBYTE * 20);
    debug_heap(stdout, heap);
    if (count_blocks(heap) != 2){
        _free(block_1);
        _free(block_2);
        printf("Amount of allocated block is incorrect\n");
        return false;
    }
    _free(block_1);
    _free(block_2);
    return true;
}

static bool test_5(struct block_header* heap) {
    debug_heap(stdout , heap);

    void *block_1 = _malloc(32*KILOBYTE);
    if (block_1 == NULL) {
        _free(block_1);
        return false;
    }
    debug_heap(stdout , heap);
    struct block_header *addr = heap;
    while (addr->next != NULL) addr = addr->next;

    void* test_addr = (uint8_t*) addr + size_from_capacity(addr->capacity).bytes;
    test_addr = map_pages(test_addr, 10*KILOBYTE, MAP_FIXED);

    if (test_addr == NULL)
        return false;

    void *data2 = _malloc(10*KILOBYTE);

    debug_heap(stdout, heap);


    _free(data2);
    return true;
}



void run_tests() {
    struct block_header *heap = (struct block_header*) heap_init(30*KILOBYTE);
    for(size_t i = 0; i < 5; i++) { //not necessary, just for fun
        printf("\n----------Started test %zu---------- \n", i+1);
        if (tests_array[i](heap))
            printf("TEST PASSED\n\n");
        else
            printf("\nTEST %zu IS NOT PASSED\n\n" , i+1);
    }
}