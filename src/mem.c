#define _DEFAULT_SOURCE
#define _GNU_SOURCE


#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"



///void *HEAPPP = ((void*)0x02200000);
void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    size_t region_size = region_actual_size( size_from_capacity((block_capacity) {.bytes = query}).bytes);
    //MAP_FIXED_NOREPLACE cannot is not defined on my kernel ;( intel macOS btw
    void* mapped_region = map_pages( addr, region_size, MAP_FIXED );
    printf("The address of map is %p\n", mapped_region);
    if (MAP_FAILED == mapped_region) {
        mapped_region = map_pages( addr, region_size, 0 );
        printf("The address of map is %p\n", mapped_region);
//        HEAPPP = mapped_region;
        if(MAP_FAILED == mapped_region )
            return REGION_INVALID;
    }
    block_init( mapped_region, (block_size){region_size}, NULL );

    return (struct region) {.addr = mapped_region , .size = region_size , .extends = mapped_region == addr};

}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
    const struct region region = alloc_region( HEAP_START, initial );
    if ( region_is_invalid(&region) ) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
    return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if(!block_splittable(block , query))  return false;

    //dividing block by two parts
    block_size initial_part_size = size_from_capacity((block_capacity){ size_max(query, BLOCK_MIN_CAPACITY)});
    block_size second_part_size = size_from_capacity((block_capacity){(block->capacity).bytes - initial_part_size.bytes});


    void* second_part_ptr = (uint8_t *)block + initial_part_size.bytes;

    block_init(second_part_ptr , second_part_size , block->next);
    block->capacity = capacity_from_size(initial_part_size);
    block->next = second_part_ptr;
    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ) {
    return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

// if blocks are neighbours => merge
static bool try_merge_with_next( struct block_header* block ) {
    struct block_header* next = block->next;
    if (next== NULL || !mergeable(block , next) )
        return false;
    block->capacity.bytes = block->capacity.bytes + size_from_capacity(next->capacity).bytes;
    block->next = next->next ;
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    struct block_search_result result = {BSR_REACHED_END_NOT_FOUND, NULL};
    while (block != NULL) {
        result.block = block;
        if (block->is_free) {
            while(try_merge_with_next(block)){};
            if (block_is_big_enough(sz, block)) {
                result.type = BSR_FOUND_GOOD_BLOCK;
                return result;
            }
        }
        block = block->next;
    }
    if (result.block == NULL) result.type = BSR_CORRUPTED;
    return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result result = find_good_or_last(block , query); //merged free blocks to reuse
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(result.block, query); // optimise mem usage
        result.block->is_free = false;
    }
    return result;

}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    if (!last) return NULL;
    struct region region = alloc_region(block_after(last), query);
    last->next = region.addr;
    if (region.extends && last->is_free)
        try_merge_with_next(last);
    return region.extends && last->is_free ? last : region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result result = try_memalloc_existing(query, heap_start);
    if( result.type == BSR_REACHED_END_NOT_FOUND) {
        grow_heap(result.block, query);
        result = try_memalloc_existing(query, heap_start);
    }
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        return result.block;
    }
    return NULL;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
    if (mem == NULL) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;

    while(try_merge_with_next(header));
}
